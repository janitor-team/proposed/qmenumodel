Source: qmenumodel
Section: libs
Priority: optional
Maintainer: Ayatana Packagers <pkg-ayatana-devel@lists.alioth.debian.org>
Uploaders:
 Debian UBports Team <team+ubports@tracker.debian.org>,
 Mike Gabriel <sunweaver@debian.org>,
Build-Depends: debhelper-compat (= 13),
               cmake (>= 2.8.9),
               cmake-extras,
               libglib2.0-dev,
               qml-module-qtquick2 <!nocheck>,
               qt5-qmake,
               qtbase5-dev (>= 5.0),
               qtdeclarative5-dev (>= 5.0),
               python3:any,
               python3-dbus:native,
               python3-gi:native,
               gir1.2-glib-2.0,
               dbus-test-runner,
               libgles2-mesa-dev | libgl-dev,
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://github.com/AyatanaIndicators/qmenumodel
Vcs-Git: https://salsa.debian.org/debian-ayatana-team/qmenumodel.git
Vcs-Browser: https://salsa.debian.org/debian-ayatana-team/qmenumodel/

Package: libqmenumodel1
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
Pre-Depends: ${misc:Pre-Depends}
Description: Qt binding for GMenuModel - shared library
 Qt binding for GMenuModel that allows connecting to a menu model exposed on
 D-Bus and presents it as a list model. It can be used to expose indicator or
 application menus for applications using the Qt framework.
 .
 This package contains the shared library required by applications using
 QMenuModel.

Package: libqmenumodel-dev
Section: libdevel
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libqmenumodel1 (= ${binary:Version}),
Description: Qt binding for GMenuModel - development files
 Qt binding for GMenuModel that allows connecting to a menu model exposed on
 D-Bus and presents it as a list model. It can be used to expose indicator or
 application menus for applications using the Qt framework.
 .
 This package contains the development headers for libqmenumodel.

Package: qml-module-qmenumodel1
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libqmenumodel1 (= ${binary:Version}),
Description: Qt binding for GMenuModel - QML module
 Qt binding for GMenuModel that allows connecting to a menu model exposed on
 D-Bus and presents it as a list model. It can be used to expose indicator or
 application menus for applications using the Qt framework.
 .
 This package contains the QML module for building applications using the
 QMenuModel library.

# remove for bookworm+1
Package: qmenumodel-qml
Section: oldlibs
Architecture: any
Depends: ${misc:Depends},
         qml-module-qmenumodel1 (= ${binary:Version}),
Description: Qt binding for GMenuModel - QML module (transitional package)
 Qt binding for GMenuModel that allows connecting to a menu model exposed on
 D-Bus and presents it as a list model. It can be used to expose indicator or
 application menus for applications using the Qt framework.
 .
 This package facilitates the transition from qmenumodel-qml to
 qml-module-qmenumodel.
